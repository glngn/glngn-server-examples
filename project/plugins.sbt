// lazy val `sbt-glngn` = RootProject(file("../../project/dependencies/sbt-glngn"))
// dependsOn(`sbt-glngn`)

resolvers += Resolver.url(
  "glngn server releases",
  url("https://glngn-server-releases.s3.amazonaws.com/releases")
)(Resolver.ivyStylePatterns)

addSbtPlugin("glngn" % "sbt-glngn" % "0.6.8")
