## `semicolon is missing before`...

### Example Case

~~~
value *> is not a member of glngn.server.node.ServiceFragmentRouter.Endpoints[Unit]
possible cause: maybe a semicolon is missing before `value *>'?
    } *> command.subpath("_ops").subpath("groups").value[GroupDesc] { groupDesc =>
      ^
~~~

### Solution

~~~
import cats.syntax.apply._
~~~

## `could not find Lazy implicit value of type ConfiguredAsObjectEncoder`

### Example Case

~~~
could not find Lazy implicit value of type
io.circe.generic.extras.encoding.ConfiguredAsObjectEncoder[Group.Enable]
[error]       CirceSemiauto.deriveConfiguredEncoder[Group.Enable]
~~~

## Solution

The configuration for circe deriving is not imported. To import the default
for glngn server:

~~~
import glngn.server.prelude._
~~~
