# Sharing - Security - Authentication and Authorization

**NOTE:** Unreleased. Contact support for further information.

A standalone run, by default, glngn server runs in insecure mode. This means no authentication or authorization is performed. All access is anonymous.

A fixed or kubernetes deployment, by default, runs in secure mode. This means authentication and authorization are performed. All access is either anonymous and authorized to only explicit public endpoints. Or the access is authenticated and authorized according to group and service instance the endpoint is under.

## Insecure and Secure mode


In secure mode glng server performs authorization and authentication. The rules are designed to be
predictable and familiar. EG: Given an endpoint path, the necessary authentication is clear from the
parts of the path; and the options resemble those of a network file server.

Authentication and Authorization (AuthN/AuthZ) in glngn server is based around:

- All accessors are Anonymous or Users
- Groups contain Users
- Users in Groups are all Members of that Group
- Specific Users in a group are Operators
- All endpoints are, by default, only authenticated for Group Members
- All `_ops` endpoints are only authenticated for Operators of the Group

## Prerequisites

In order to use authentication or authorization:

- Google is the only authentication provider supported at this time
- For more than 5 operators, a license key is required. Contact [support@dogheadbone.com](mailto:support@dogheadbone.com) for further information

