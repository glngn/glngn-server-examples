# Customization / Extensions

glngn server provides a programmable application experience: The configuration and new service
fragments can be defined using Scala code.

## prerequisites:

- scala 2.12
- sbt 1.x
- agreement to the [Developer License](https://gitlab.com/glngn/glngn-server-examples/blob/master/docs/Developer%20License%20v1.rtf) This license is also included in the jar archive.

The recommendation is to use the `sbt-glngn` sbt plugin. Very little code is required to get
started. The plugin automatically includes all necessary dependencies, tasks and settings in the
project.

# Guide

[The glngn-server-examples repository](https://gitlab.com/glngn/glngn-server-examples/-/tree/master)
is a guided series of customizations. These start with no customization, `starter-0`, and progress
with customizations of increasing complexity.

## Checkout and Build Examples

1. git checkout the examples repo


```
git clone https://gitlab.com/glngn/glngn-server-examples.git
```

2. start an [sbt](https://www.scala-sbt.org/) session
3. compile the examples with `compile`

```
sbt:server-examples> compile
[info] Updating starter-1
https://glngn-server-releases.s3.amazonaws.com/assemblies/glngn/glngn-server-assembly_2.12/0.6.8/ivys/ivy.xml
100.0% [##########] 2.1 KiB (4.7 KiB / s)
[info] Resolved starter-1 dependencies
...
...
sbt:server-examples>
```

## Examples Overview

Each example is a separate project in sbt:

1. `starter-0` - a glngn server with the default configuration. This is the version [described
   in the user manual](https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md)
3. `starter-1` - a glngn server that has a group and service fragment instantiated by default.
4. `k8s-0` - same as `starter-1` and configured for clustering. This uses Kubernetes and Amazon DynamoDB
to provide a high scalability and high reliability deployment

## `starter-0`

The `starter-0` example is identical to the default distribution [described in the user
guide](https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md).

Note the `starter-0` directory is empty. No source is required: the default build of a `sbt-glngn`
project is identical to the default configuration.

We can verify this by building and running the assembly:

```
$ sbt starter-0/assembly
[info] Set current project to starter-0 (in build file:/home/coconnor/Development/glngn/server-examples/)
[info] Strategy 'deduplicate' was applied to 3912 files (Run the task at debug level to see details)
[info] Strategy 'discard' was applied to a file (Run the task at debug level to see details)
[info] Strategy 'rename' was applied to 2 files (Run the task at debug level to see details)
[success] Total time: 32 s, completed Mar 27, 2020 2:56:11 PM
$ java -jar ./starter-0/.jvm/target/scala-2.12/starter-0-assembly.jar --help
glngn server 0.1.0
Usage: glngn-server [options] [command] [command options]
  --port  <int?>
...
```

The resulting jar has the same interface as the default.

## `starter-1`

The `starter-1` example is pre-configured to have a group and service fragment. The result is
equivalent to the service built by "Instantiate a service fragment" [in the user
guide](https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md).

We can verify this by building the assembly; running with a fresh state; verifying the OpenAPI schema does, in fact, contain
the group and service already.

```
$ sbt starter-1/assembly
[...]
[success] Total time: 15 s, completed Apr 6, 2020 4:34:39 PM
$ java -jar starter-1/.jvm/target/scala-2.12/starter-1-assembly.jar run --state-dir ./starter-1.state --port 10000
[...]
DEBUG glngn.server.host.Logging$  - logging service loaded
INFO  glngn.server.host.Logging$  - use of this software is agreement with the included Application Developer License.
INFO  org.apache.pekko.event.slf4j.Slf4jLogger  - Slf4jLogger started
[...]
INFO  glngn.server.host.HostApp$  - application is listening at http://localhost:10000
```

Will serve a schema on `localhost:10000/openapi.json` like:

![starter-1 OpenAPI schema](https://gitlab.com/glngn/glngn-server-examples/-/raw/master/docs/starter-1-openapi.png?inline=false)

The differences between defining groups and services by overriding `HostApp` and using the `_ops` APIs dynamically are:

1. The group and services are enabled at compile time. No additional runtime config is required to
   access the group or service.
2. The group and service cannot be removed. This ensures all users of this deployment can always
rely on these services.

Internally, glngn server can handle a mix of dynamic and static configuration. This enables the
server to deploy updates into an existing cluster: There is no requirement to commit to a static or
dynamic configured server. For example, feel free to start with a dynamic
configuration and migrate to encoding a static configuration. This migration can be handled
without downtime.

## `k8s-0`

This example demonstrates [configuring for kubernetes deployment](docs/KubernetesDeployment). This
example does not work without additional configuration. Specifically:

- AWS credentials for an IAM account with Read/Write to a DynamoDB table
- DynamoDB table configured appropriately
- Kubernetes cluster with OpenShift available

Dogheadbone LLC has a demo Kubernetes deployment. Please [contact us](mailto:support@dogheadbone.com)
for a deep dive session.

# Reference

## `sbt-glngn`

### Tasks

- `~ reStart`

Starts the service on port 10000 (by default). Restarts the service on code changes.

### New Project Setup

For a new project named, for example, `my-glngn` the build setup is:

- `project/plugins.sbt`:

~~~
resolvers += Resolver.url(
  "glngn server releases",
  url("https://glngn-server-releases.s3.amazonaws.com/releases")
)(Resolver.ivyStylePatterns)

addSbtPlugin("glngn" % "sbt-glngn" % "0.6.8")
~~~

- `build.sbt`:

~~~
lazy val `my-glngn` =
  glngn.sbt.StarterDeployment(
    "my-glngn",
    file("my-glngn"),
  )
~~~

- `project/build.properties`:

~~~
sbt.version=1.3.3
~~~
