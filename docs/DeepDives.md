# Deep Dives

## An Initialized Server

The properties of an initialized server:

- All services defined in the prelude are initialized and available
- No critical errors occurred instantiating all dynamically instantiated services
- The `openapi.json` route is available and contains endpoint documentation for
  all services w/o endpoint definition errors
- A typed pekko cluster is available with, by default, 3 members
- The `healthz` endpoint returns 200 OK
- The `healthz` endpoint on the operations (ops) port returns 200 OK
- Pekko Management is available on the operations (ops) port

## Standalone Server Initialization

A standalone server is a single process server. The standalone server can be used
to model services, host services, and store events the same as the cluster
server.


This process is hosting multiple cluster nodes.
While each node is hosted in the same JVM instance there is no assurance
of separate class loaders. There is also no assurance of a single pekko
system instance. With that in mind, on startup:

1. a minimal pekko system is initialized for management
2. the configuration for the cluster nodes is determined. This includes logging,
   pekko system, and storage.
3. the requested initial node count of nodes are started
4. the application waits for configuration from each node of initialization
5. Either all nodes initialize within the required time or the application
   exits with an error

## On the use of an assembly

The default build uses an assembly, non-modular, glngn server library. This
provides a low complexity build for extensions that only need the included
libraries. See [the API documentation here](http://docs.glngn.com/latest/api/)
for reference.

Assembly distributions are considered difficult "the moment your users step
outside of Hello World". However, with glngn server those simple cases are
useful and the convinience is valuable.

In addition, the provided glngn server assembly has a high degree of assurance
that is difficult to provide with a modular build. The
assembly is built and verified against specific versions of dependent libraries.


There is a modular build that  permits adding or changing dependents. This does
invalidate the prior assurances to a degree. Contact us for further information.

## Service Fragment Instantiation

### Service Fragments with the same service ID

Multiple service fragments may be instantiated under a service ID. This is subject to the
conditions:

1. Each service fragment must have a unique logical ID.
2. The service fragment endpoints must be disjoint
3. The service fragment event schema must be disjoint

### Can any class be instantiated?

No. While the instantiation operation accepts a class name this must be in the whitelisted set of
classes provided in the `Prelude`.

This set can only be changed by building a customized version of glngn server and using
that build.
