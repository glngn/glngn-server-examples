# Kubernetes Deployment

We've designed glngn server for simple operation using kubernetes. Both standalone usage and
clustered usage are supported.

Features:

- automatic node removal for terminated containers
- bootstrapping and lookup, using pekko management, is preconfigured
- memory and threadpools tuned for containers
- provided routes for liveness and readiness probes
- separate port for internal cluster operations

### Container Image

- Projects using `sbt-glngn` include
  [`sbt-native-packager`](https://github.com/sbt/sbt-native-packager/) for building a container
  image.
- Entry point is a script that launches the project's `mainClass`.
- Default command is `--help`.

See the [README-containers](./README-containers.md) for details on the container interface.  See
[ImageBuild](./ImageBuild.md) for details on the provided image build.

## Service Definition

(TODO: include openshift definition)

## Route Definition

(TODO: include openshift definition)

## Storage

glngn server uses akka persistence for storage. The standalone mode is preconfigured to use
LevelDB. The kubernetes deployment mode supports Amazon's DynamoDB.

Deep Dive: Internally, pekko persistence is used. The value add of glngn server is simplified
configuration and operations. At this time only DynamoDB and LevelDB are supported. Contact
[support@dogheadbone.com](mailto:support@dogheadbone.com) for information on supporting other pekko
persistence backends.

### DynamoDB

With a deployment name `$NAME` in AWS `$REGION`:


1. in `application.conf`:

~~~
glngn.server {
    default-persistence = "dynamodb"

    dynamodb {
        URL = "https://dynamodb.$REGION.amazonaws.com"
        signing-region = "$REGION"
        journal-table = "$NAME-journal"
        snapshot-table = "$NAME-snapshot"
    }
}
~~~

2. create DynamoDB tables `$NAME-journal` and `$NAME-snapshot`.

journal:

- partition key (hash key) of: name "par" ; type string
- sort key (range key) of: name "num" ; type number
- "table settings": default settings are fine to start. on-demand provisioning is supported.

snapshot:

- hash key of string type "par"
- range key of number type "seq"
- local secondary index, projecting all attributes, named "ts-idx" of:
- hash key of string type "par"
- range key of number type "ts"

The schema and existence of the tables will be verified on server startup. The server will not
initialize if the tables are invalid.
