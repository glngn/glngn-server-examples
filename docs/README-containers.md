# glngn-server container image

The standard distribution of `glngn-server` is available as a container image:

- https://hub.docker.com/repository/docker/dogheadbone/glngn-server

This image supports both standalone and clustered use. The mode depends on environment and command options.

Projects using the [sbt-glngn plugin of the
SDK](https://gitlab.com/glngn/glngn-server-examples/-/blob/master/docs/Customization.md)
automatically include a configuration for generating similar container images.

See the [User Manual](https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md) for usage
of `glngn-server`.

## Quick Start

The default command is `--help`:

```bash
$ docker run glngn-server:latest
glngn-server HEAD
Usage: glngn-server [options] [command] [command options]
  --port  <int?>
  --disable-ops  <bool>

Commands: print-config; print-full-config; run; version

Command: print-config
Usage: glngn-server print-config

Command: print-full-config
Usage: glngn-server print-full-config

Command: run
Usage: glngn-server run
  --state-dir  <string?>
  --exit-immediately  <bool>
  --initial-node-count  <int>
  --heap-dump-on-init  <bool>
Command: version
Usage: glngn-server version

Use of this software is agreement with the included Application Developer License.
```

To `run` an ephemeral instance accessible on port 10501 using HTTP:

```bash
$ docker run --publish 10501:10501 glngn-server:latest run
```

The following sections cover running the container image in Standalone or Clustered mode. Once
running, see the [User Manual](https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md)
for usage of `glngn-server`.

This interface can be customized or replaced using the SDK. In particular, the HostApp API.

## Standalone Mode

By default, `run` will run standalone mode using the container's temporary storage and the server
port 10501:

```bash
$ docker run --publish 10501:10501 glngn-server:latest run
DEBUG glngn.server.host.Logging$  - logging service loaded
INFO  glngn.server.host.Logging$  - use of this software is agreement with the included Application Developer License.
INFO  org.apache.pekko.event.slf4j.Slf4jLogger  - Slf4jLogger started
[...]
INFO  glngn.server.host.HostApp$  - application is listening at http://localhost:10501
```

To persist the data after the container exits use a docker volume. By default, mount a volume to `/var/lib/glngn`.

For example, to create a volume `glngn.state` and run `glngn-server` in standalone mode use:

```bash
$ docker run --publish 10501:10501 --volume glngn.state:/var/lib/glngn glngn-server:latest run
DEBUG glngn.server.host.Logging$  - logging service loaded
INFO  glngn.server.host.Logging$  - use of this software is agreement with the included Application Developer License.
INFO  org.apache.pekko.event.slf4j.Slf4jLogger  - Slf4jLogger started
[...]
INFO  glngn.server.host.HostApp$  - application is listening at http://localhost:10501
```

## Clustered Mode

### DynamoDB Schema

In clustered mode the DynamoDB persistence must be used. This requires two tables: A journal and a snapshot table.

**Journal Table**:

- Partition key / hash key: name "par" ; type string
- Sort key / range key: name "num" ; type number
- Table settings: default settings are fine to start. on-demand provisioning is supported.
- See `GLNGN_DYANOMDB_JOURNAL_TABLE` in Configuration

**Snapshot Table**:

- Hash key of string type "par"
- Range key of number type "seq"
- Local secondary index: project all attributes; named "ts-idx" of:
  - Hash key of string type "par"
  - Range key of number type "ts"
- Table settings: default settings are fine to start. on-demand provisioning is supported.
- See `GLNGN_DYANOMDB_SNAPSHOT_TABLE` in Configuration

The schema and existence of the tables will be verified on server startup. The server will not
initialize if the tables are invalid.

## Configuration

The provided container image uses environment variables as the primary configuration mechanism.
Developers can use the provided SDK to create container images with a fixed or extended
configuration.

The environment variables, information on these, and the corresponding config property to override
when using the SDK:

- GLNGN_MODE
  - *default:* infer
  - *options:*
    - *infer* - The mode is inferred from the environment. If all required parameters for clustered mode are provided
    then clustered mode is selected. If there are both standalone and clustered arguments provided then the process
    fails with a non-zero status code.
    - *standalone* - Standalone is used. Clustered mode parameters are ignored.
    - *clustered* - Clustered mode is used. Standalone parameters are ignored.
  - *config property* - none
  - *command line argument* - none
- GLNGN_STANDALONE_DEFAULT_PORT
  - *default:* 10501
  - *options:*
    - *0* - An arbitrary, fresh port is selected.
    - *integer value* - available port to use. If port cannot be used then the process fails with non-zero status code.
  - *config property* - none
  - *command line argument* - `--port`
- GLNGN_STANDALONE_DEFAULT_STATE_DIR
  - *default:* Directory `glngn.state` in current working directory. Assuming defaults, this is `/var/lib/glngn/glngn.state`.
  - *options:* Absolute path to **directory** that will be used for storage. EG: Path for a persistent volume claim.
  - *config property* - none
  - *command line argument* - `--state-dir`
- GLNGN_CLUSTER_SERVER_PORT
  - *default:* 10501
  - *options:*
    - *0* - An arbitrary, fresh port is selected.
    - *integer value* - available port to use. If port cannot be used then the process fails with non-zero status code.
  - *config property* - `glngn.server.server-port`
  - *command line argument* - none
- GLNGN_CLUSTER_OPS_PORT
  - *default:* 10502
  - *options:*
    - *0* - An arbitrary, fresh port is selected.
    - *integer value* - available port to use. If port cannot be used then the process fails with non-zero status code.
  - *config property* - `glngn.server.ops-port`
  - *command line argument* - none
- GLNGN_CLUSTER_CLUSTER_PORT
  - *default:* 10503
  - *options:*
    - *0* - An arbitrary, fresh port is selected.
    - *integer value* - available port to use. If port cannot be used then the process fails with non-zero status code.
  - *config property* - `glngn.server.cluster-port`
  - *command line argument* - none
- GLNGN_DYNAMODB_URL
  - *no default*
  - *options:* URL for dynamodb server. EG: `https://dynamodb.us-east-1.amazonaws.com`
  - *config property* - `glngn.server.dynamodb.URL`
  - *command line argument* - none
- GLNGN_DYNAMODB_SIGNING_REGION
  - *no default*
  - *options:* Region to use for signing requests. EG: `us-east-1`
  - *config property* - `glngn.server.dynamodb.signing-region`
  - *command line argument* - none
- GLNGN_DYNAMODB_JOURNAL_TABLE
  - *no default*
  - *options:* Name of DynamoDB table to use for journal. EG: `beta-journal`
  - *config property* - `glngn.server.dynamodb.journal-table`
  - *command line argument* - none
- GLNGN_DYNAMODB_SNAPSHOT_TABLE
  - *no default*
  - *options:* Name of DynamoDB table to use for snapshots. EG: `beta-snapshot`
  - *config property* - `glngn.server.dynamodb.snapshot-table`
  - *command line argument* - none
- GLNGN_DEFAULT_PERSISTENCE
  - *default:* infer
  - *options:*
    - *infer* - Select persistence based on mode.
    - *inmem* - No persistence; persist to memory only.
    - *state-dir* - Use state directory persistence. Only valid in standalone mode.
    - *dynamodb* - Use DynamoDB persistence.
  - *config property* - `glngn.server.default-persistence`
  - *command line argument* - none

See also the `reference.conf` included in the `glngn-server` package.

## Commands

### `--help`

### `run`

### `print-config`

### `print-full-config`
