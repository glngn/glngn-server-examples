# Container Image Build

Projects using `sbt-glngn` are pre-configured to generate docker images using
[`sbt-native-packager`](https://github.com/sbt/sbt-native-packager/):

```
sbt myProject/docker:publishLocal
[info] Sending build context to Docker daemon  121.7MB
[info] Step 1/33 : FROM openjdk:11 as stage0
...
[info] Built image myProject with tags [latest]
```

This is how the image for the
[`glngn-server`](https://hub.docker.com/repository/registry-1.docker.io/dogheadbone/glngn-server)
distribution is created.

## Versions and Tags

## Customizing

The following variables should be used to change the exposed ports:

```scala
containerServerPort := 10501
containerOpsPort := 10502
containerClusterPort := 10503
```

See [`sbt-native-packager`](https://github.com/sbt/sbt-native-packager/) for further
docker image options.
