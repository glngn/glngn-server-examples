0.8.0 - unreleased

- Replace the required `Instance` class definition with a requirement for a `create: InstanceRef`
  definition. This is an IO method that is constructed by `instantiate`. This can contain any number
  of `Instance.endpoints` and `Instance.bindings`.

    - example using the prior API <br> ![image.png](./docs/0-8-api-change-ex-0.png)
    - example using the new api <br> ![image_1.png](./docs/0-8-api-change-ex-1.png)

0.6.0

- ZIO updated to 1.0.0-RC19-2

0.5.0

- The classes `glngn.server.host.DynamicConfigHost` and `glngn.server.host.FixedConfigHost`
  have been unified under `glngn.server.host.HostApp`.
- The provided prelude has been re-organized. Two additional prelude variations
  are provided: `glngn.server.prelude.self` for a prelude containing only glngn server aspects.
  `glngn.server.prelude.external` for a prelude containing types and values external
  to glngn server that are commonly used. `glngn.server.prelude` is exactly these
  two combined.

0.4.0

- This release is the first release to include the dynamic service building features. Built-in and
  custom components (ServiceFragments) can be dynamically instantiated under groups. All of which is
  persistent and reflected in the OpenAPI interface.
- 0.4.x release adds fixes, performance and features to the API:
    - The ServiceEntity API has been re-done. Easily supports IO actions as protocol message handlers
    - The bindings interface includes easy delegation to IO functions and non-IO functions
- smaller size and improved performance. Lowers the required metaspace. Improved startup performance and delegate dispatch.
