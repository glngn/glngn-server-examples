glngn server is an extensible business process application server. Similar to
Microsoft Access or Apple FileMaker, but designed for event sourced business services with a code-first philosophy.
A standalone application and an approachable SDK is provided.  Applications developed with glngn
server can be easily deployed to a kubernetes cluster.

- Support available from the [DogHeadBone LLC](mailto:support@dogheadbone.com) company
- Useful built-in services
- Easy event captures
- Predictable REST interface with an OpenAPI schema
- A [high level extension API](http://docs.glngn.com) for Scala service developers.
  (Plain, typed pekko and zio interfaces are provided.)
- Simple persistence features
- Simple Kubernetes (k8s) cluster deployments. In addition to some custom automation, pekko
  management tools are included. OpenShift is also supported.

Sound good so far? Excellent! Let's start with basic, interactive usage of the standalone application.

# Developer SDK

For a guide to the SDK see:

- [development guide](docs/Customization.md)
- [development error help](docs/CompileErrorHelp.md)

The SDK API is familiar to Scala service developers. To a large degree,
glngn server is a friendly, constrained, pre-configured typed pekko plus ZIO server. A few of the
niceties included:

- Clustering is enabled and established without additional configuration
- Logging and data marshalling are configured appropriately
- Additional error tracking and handling

# Standalone Application

prerequisites:

- java runtime version 8 or above.
- agreement to the [Developer License](docs/Developer%20License%20v1.rtf?inline=false) This license is also included in the jar archive.

## Download

Prior to downloading, read and agree to the [Developer License agreement](docs/Developer%20License%20v1.rtf?inline=false).

- download [the latest release](https://glngn-server-releases.s3.amazonaws.com/assemblies/glngn/glngn-server-assembly_2.12/0.6.8/jars/glngn-server-assembly_2.12-assembly.jar)

```bash
curl -L -o glngn-server-assembly.jar https://glngn-server-releases.s3.amazonaws.com/assemblies/glngn/glngn-server-assembly_2.12/0.6.8/jars/glngn-server-assembly_2.12-assembly.jar
```

## Command Help

Run using `java` and pass `--help` to see usage information:

```bash
$ java -jar ./glngn-server-assembly.jar --help
Usage: glngn-server [options] [command] [command options]
  --port  <int?>
  --disable-ops  <bool>

Commands: print-config; print-full-config; run; version

Command: print-config
Usage: glngn-server print-config

Command: print-full-config
Usage: glngn-server print-full-config

Command: run
Usage: glngn-server run
  --state-dir  <string?>
  --exit-immediately  <bool>
  --initial-node-count  <int>
  --heap-dump-on-init  <bool>

Command: version
Usage: glngn-server version

Use of this software is agreement with the included Application Developer License.
```

## Running

Note: For consistency the docs always use port 10000. This is requested using the `--port 10000`
command line argument.

Let's run a server configured to store data at `./starter.state`. In a terminal session:

```bash
$ java -jar ./glngn-server-assembly.jar --port 10000 run --state-dir ./starter.state
INFO  use of this software is agreement with the included Developer License.
...
[lots of output]
...
INFO  glngn.server.host.ServerApp$  - application is listening at http://localhost:10000
```

The standalone server is initialized and ready.

See [docs/Deep Dives](https://gitlab.com/glngn/glngn-server-examples/blob/master/docs/DeepDives.md)
for technical details on the initialization process.

Hit control-C, or TERM the process (there is only one), to shut down the server. This will take a bit
for a nice, coordinated shutdown.

## Usage

The glngn server provides a REST API with an [OpenAPI](https://www.openapis.org/) specification.
This API supports the built-in features of glngn server as well as any additional features added by
extensions.

This guide walks through the API using `curl`.  If you are looking for a GUI or integration with
another HTTP client [please let us know](mailto:support@dogheadbone.com).

### OpenAPI schema

Let's query for the API schema to see what endpoints there are:

```bash
$ curl http://localhost:10000/openapi.json
{
  "openapi" : "3.0.1",
  "info" : {
    "title" : "dynamic",
    "version" : "0.1"
  },
  "paths": {
    "/healthz": ...
    "/_ops/groups": ...
    "/_ops/services": ...
[...]
  }
}
[...]
```

A brief overview:

1. The `/healthz` route. We'll try that one below.
2. The `/_ops/groups` route. This is an operations route for the groups in glngn server. All services
are placed under a group. The default configuration does not include any groups. This
will be the first route we try in the `API Tour`.
3. The `/_ops/services` route. This is an operations route to query the services that are available
to instantiate under a group. This will be covered after the `/_ops/groups` route.

### Multiple Representations

The `/healthz` route will only return status 200 OK if the server is able to handle requests.
This is also a nice example to demonstrate how glngn server supports multiple representations:

```bash
$ curl http://localhost:10000/healthz.txt
OK
```

Note the use of a `.txt` extension to request a text representation. We could have
requested another representation, such as `json`, explicitly or implicitly:

```bash
$ curl -H 'Accept: text/plain' localhost:10000/healthz
OK

$ curl -H 'Accept: application/json' localhost:10000/healthz
{
  "memStats" : {
[...]
  "ok" : true
}
$ curl localhost:10000/healthz.json
{
  "memStats" : {
[...]
  "ok" : true
}
```

This pattern is supported throughout: Endpoints will have suffixed versions that imply the expected
representation.

### API Tour

The API conventions enforced by glngn server:

- All routes prefixed with a `_` are operations routes. These are provided by glngn server.
- `/_ops/`
    - Deployment `_ops` routes
    - These are always available
    - EG: `/_ops/groups`: query and modify the available groups
- `/$GROUP/_ops`
    - Group `_ops` routes. These require a group with id, `$GROUP`, to be created
- `/$GROUP/$SERVICE/_ops`
    - Service fragment `_ops` routes, `/$GROUP/$SERVICE/_ops`, require the service fragment to be
    instantiated under the group
    - automatically generated for each instantiated service fragment
- `/$GROUP/$SERVICE/...`
    - All other routes under a service are provided by a Service Fragment's `endpoints`

#### 1. Create a Group

The `/_ops/groups` endpoint is used to query the existing groups and create new groups.  All services
are namespaced under a group. Before we can instantiate and use a service we must first create a
group:

~~~
$ curl --data '{ "id": "accounts", "name": "Client Accounts" }' localhost:10000/_ops/groups
{
  "enabled" : true,
  "id" : {
    "txt" : "accounts"
  },
  "name" : "Client Accounts",
  "operators" : null
}
~~~

This creates a group with the logical name `accounts`. The endpoint responds with the
current definition of the group.

Which we can confirm by querying the `/_ops/groups` endpoint again:

~~~
$ curl localhost:10000/_ops/groups
{
  "accounts" : {
    "enabled" : true,
    "id" : {
      "txt" : "accounts"
    },
    "name" : "Client Accounts",
    "operators" : null
  }
}
~~~

Note the `operators` attribute. Using the premium license server this would contain the users
authorized to be operators in that group. The free license server ignores this attribute.

#### Groups Extend the API

By creating a group we've extended the provided API. Query the `openapi.json` route again
and note the `/accounts/_ops` route:

~~~
$ curl localhost:10000/openapi.json
{
  "openapi" : "3.0.1",
  "info" : {
    "title" : "dynamic",
    "version" : "0.1"
  },
  "paths" : {
    "/accounts/_ops" : {
      "get" : {
        "operationId" : "getAccounts_ops",
        "responses" : {
          "200" : {
            ...
          }
        }
      }
    }
  }
}
~~~

##### Note: Groups Persist

If you were to restart the `java -jar ./glngn-server-assembly.jar` process note the group
remains. `glngn-server` persists the created group to the provided state storage.

In the example above the state storage is the `starter-state` directory provided to the
`--state-dir` option.

#### Instantiate a Service Fragment

Now that a group exists we will instantiate a service under that group. This will:

1. Extend the API to include that service.
2. Persist the data for that service under the group and service namespace.

##### 1. Query for available services

To determine what services fragments we can instantiate query the `/_ops/services` route:

~~~

$ curl localhost:10000/_ops/services.json
[
  "glngn.server.services.StringValues$",
  "glngn.server.services.IntegerCounters$"
]
~~~

This provides the list of logical IDs of service fragments that can be instantiated under a group.
This set can be customized using the developer SDK. We are using the default glngn distribution which
includes a standard set of generic service fragments.

See also: [DeepDives - service fragment instantiation](DeepDives#service-fragment-instantiation)

##### 2. Instantiate the service fragment

Let's pick the `glngn.server.services.IntegerCounters$` service fragment to instantiate.

After creating the group `accounts` a route was added `/accounts/_ops/services`. According to the
OpenAPI spec this route accepts a `POST` of a `GroupServiceChange` json structure. The schema of
this structure is:

~~~
"GroupServicesChange" : {
    "required" : [
        "serviceId",
        "logicalServiceId"
    ],
    "type" : "object",
    "properties" : {
        "serviceId" : {
            "type" : "string"
        },
        "logicalServiceId" : {
            "type" : "string"
        }
    }
}
~~~

We already know the logical service ID (`logicalServiceId`) from the query to `/_ops/services`. The
`serviceId` is an identifier we can pick ourselves. We will pick `retail` for this example.

~~~
$ curl --data '{ "serviceId": "retail", "logicalServiceId": "glngn.server.services.IntegerCounters$" }' localhost:10000/accounts/_ops/services
{
  "retail" : [
    "glngn.server.services.IntegerCounters$"
  ]
}
~~~

Which replies with the new set of services under that group. This reply states the `retail` service
is a composition of a list of service fragments. Which only contains
`glngn.server.services.IntegerCounters$` in this example.

See [docs/Deep Dives](https://gitlab.com/glngn/glngn-server-examples/blob/master/docs/DeepDives.md) for
details.

#### Use a Service

With a service fragment instantiated under a group we can note the OpenAPI schema now includes routes
for the `/accounts/retail` service:

##### The Extended OpenAPI Schema

With this fragment instantiated under `/accounts/retail` the service's routes are now visible in the
OpenAPI schema:

~~~
$ curl localhost:10000/openapi.json
{
  "openapi" : "3.0.1",
  "info" : {
    "title" : "dynamic",
    "version" : "0.1"
  },
  "paths" : {
    "/accounts/retail/{Entity ID}" : {
      "get" : {
          ...
        }
      }
    },
    "/accounts/retail/{Entity ID}/inc" : {
      "post" : {
          ...
        }
      }
    },
    "/accounts/retail/{Entity ID}/dec" : {
      "post" : {
          ...
        }
      }
    }
  }
}

~~~

- `/accounts/retail/{Entity ID}`
    - A query, `GET`, that returns an integer given an `Entity ID`
- `/accounts/retail/{Entity ID}/inc`
    - A command, `POST`, that returns the new value of a given `Entity ID` when provided an increment amount.
- `/accounts/retail/{Entity ID}/dec`
    - A command, `POST`, that returns the new value of a given `Entity ID` when provided a decrement amount.

##### Note: Entity ID

An `Entity ID` is a common path component and identifier in glngn server. `Entity ID`s are:

- case insensitive
- case preserving
- consist only of characters in `[a-zA-Z0-9\\-_]`: Upper or lower case letters, numbers, '-' and '_'.
- all invalid characters are filtered if provided for an Entity ID
- less than 28 characters in length

New service fragments developed with the [SDK](docs/Customization.md)
can constrain the domain of identifiers.

##### Get a Counter

~~~
$ curl localhost:10000/accounts/retail/central-store
{
  "value" : 0
}
~~~

This is a query for the `retail` counter `central-store`. The result is a value of 0.

Creating the counter prior to querying was not required in this case. Counters default to 0.

For the case of counters having a default value is useful. In general, service fragments
can have entities that require a creation command.

##### Increment a Counter

Using the `inc` subroute the counter can be incremented by a given value:

~~~
$ curl --data 10 localhost:10000/accounts/retail/central-store/inc
{
  "value" : 10
}
$ curl --data 12 localhost:10000/accounts/retail/central-store/inc
{
  "value" : 22
}
$ curl localhost:10000/accounts/retail/central-store
{
  "value" : 22
}
~~~

##### Events Persist

If the glngn server application was to be restarted then this data would persist. If the counter
was queried after restart the value would be the same as before.

The server is persisting all events that modified that counter. Each of those `inc` and `dec`
used to change a counter is saved. This history is essential to "event sourcing". Having a
history, not only the current state, of a service enables a new level of analysis of your
business activity.

The standalone version persists to the directory provided to the `--state` command line argument.
When deployed to kubernetes the service can also persist to a database such as Amazon's DynamoDB
or Cassandra.

#### Operator Access

The REST endpoints above are an interface to the service's event protocol. The
service designer defines the REST endpoint as a request event and expected
response event. How this request event is handled is defined separately.

One purpose of this separation between endpoints and service protocol is to
differentiate "users" of a service from "operators" of that service. Operators
have additional access to services. Including access to the service protocol
directly. This enables operators, for example, to post requests that are
not part of the exposed endpoints.

This additional access is exposed under the "_ops" endpoints of a service
and group. We've already used a few of the operator endpoints above. Direct
access to the protocol is provided via the:

- `_ops/requests` - POST envelope containing protcol message

#### Capturing Events

If we have a model of our business events we can build and instantiate service fragments that
reflect that model. What do we do if we don't have a model of our business events? These unknowns are likely
in a number of situations: New business proposals; exceptional business events; unexpected issues. These
will not have a schema or service structure a-priori. This is why glngn server supports arbitrary
events to enable your business to start building a model.

TODO:

1. demo capturing unmodeled events:
    1. deployment-wide
    2. group events
    3. service events
2. demo capturing a service modeled event

aka schema on write

#### Requests

A request is a unique interaction with a client. Requests are implicitly
created when the endpoints are used. A request can be explicitly requested
by posting an event of type `command` or `query` to the `_ops` endpoints.

TODO:

1. Each request has a unique ID
2. Any event may be associated with a request
3. A request is resolved when a required event is provided
4. Events may still be associated with a request after resolution

#### Event History

All services have an event history.

TODO:

1. demo querying service history

#### Request History

Each request has an event history.

TODO:

1. demo querying request history
