// uncomment and place an assembly jar in lib to use that directly
// Global / glngnAssemblyDirectory := Some(file("lib/"))

// used by glngn server developers
// Global / glngnAssemblyDirectory := Some(file("../server/jvm/target/scala-2.12/"))

/* Note the "starter-0" directory is empty.
 */
lazy val `starter-0` =
  glngn.sbt.StarterDeployment(
    "starter-0",
    file("starter-0"),
  )

/* The "starter-1" directory contains only a custom HostApp. This is equivalent to
 * the server built in the user guide:
 *
 * https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md#2-instantiate-the-service-fragment
 */
lazy val `starter-1` =
  glngn.sbt.StarterDeployment(
    "starter-1",
    file("starter-1"),
    "glngn.server.example.starter.Starter1App"
  )
