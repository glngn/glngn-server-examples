package glngn.server.example.starter

import glngn.server.prelude._

/** Same as the service built in the user guide:
  *
  *    - https://gitlab.com/glngn/glngn-server-examples/blob/master/README.md#2-instantiate-the-service-fragment
  */
final object Starter1App extends HostApp[NoAppParams] {
  override val deploymentName = "starter-1"

  override def deploymentPrelude: Prelude =
    enableGroup('accounts, "Client Accounts") |+|
      enableService('accounts, 'retail, glngn.server.services.IntegerCounters)
}
